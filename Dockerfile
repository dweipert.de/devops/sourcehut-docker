ARG VERSION

FROM "alpine:${VERSION}"

ARG VERSION
ARG SERVICES

RUN \
	sed -i "1i https://mirror.sr.ht/alpine/v${VERSION}/sr.ht" /etc/apk/repositories; \
	wget -q -O /etc/apk/keys/alpine@sr.ht.rsa.pub https://mirror.sr.ht/alpine/alpine@sr.ht.rsa.pub; \
	apk update

RUN apk add meta.sr.ht
RUN apk add ${SERVICES}

CMD [ "/bin/sh" ]

